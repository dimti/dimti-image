Changelog
=========

0.0.21 (2014-05-07)
-------------------

- Merge branch 'master' of dimti.ru:lib/Image. [Alexander Demidov]

0.0.20 (2014-05-07)
-------------------

- Откат детайлей. [Alexander Demidov]

0.0.19 (2014-04-03)
-------------------

- Удаление push_update.sh. [Alexander Demidov]

0.0.18 (2014-02-18)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

- MIRSPORTA-255 Добавление/редактирование цветов в карточке товара.
  [Alexander Demidov]

0.0.17 (2014-02-18)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

- MIRSPORTA-260 Отсутствующие картинки на 10баллов "обход" ошибки с
  отсутствующим файлом изображения при попытке форсированного создания
  варианта push-update. [Alexander Demidov]

0.0.16 (2014-01-24)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

- MIRSPORTA-80 Проверка существования метода getWatermark перед его
  использованием. [Alexander Demidov]

0.0.15 (2014-01-13)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

- Правка push-update Исправление метода File::getOwner() [Alexander
  Demidov]

0.0.14 (2014-01-13)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

- Правки push-update  - расстановка фигурных скобок в удобочитаемом и
  корректном виде. [Alexander Demidov]

0.0.13 (2014-01-13)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

- Правки push-update  - удаление тега перед его повторным созданием.
  [Alexander Demidov]

- Update CHANGELOG.md. [Alexander Demidov]

0.0.12 (2014-01-13)
-------------------

- Правки push-update (возможно, завершающие) [Alexander Demidov]

- Update CHANGELOG.md. [Alexander Demidov]

0.0.11 (2014-01-13)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

0.0.10 (2014-01-13)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

0.0.09 (2014-01-13)
-------------------

- Update CHANGELOG.md. [Alexander Demidov]

0.0.08 (2014-01-13)
-------------------

- Правки push-update 08. [Alexander Demidov]

- Update CHANGELOG.md. [Alexander Demidov]

0.0.07 (2014-01-13)
-------------------

- Правки push-update 07. [Alexander Demidov]

- Update CHANGELOG.md. [Alexander Demidov]

0.0.06 (2014-01-13)
-------------------

- Правки push-update 06. [Alexander Demidov]

- Update CHANGELOG.md. [Alexander Demidov]

0.0.05 (2014-01-13)
-------------------

- Правки push-update 05. [Alexander Demidov]

- Update CHANGELOG.md. [Alexander Demidov]

0.0.04 (2014-01-13)
-------------------

- Правки push-update. [Alexander Demidov]

- Корректировки push-update. [Alexander Demidov]

- Правка push-update. [Alexander Demidov]

0.0.01 (2014-01-13)
-------------------

- Bash-script push-update complete and add to track. [Alexander Demidov]

- Свойство owner к ImageCollection. [Alexander Demidov]

- Свойство owner. [Alexander Demidov]

- Корректировка в связи с последними изменениями. [Alexander Demidov]

- Попытка исправить утечку оперативной памяти N2. [Alexander Demidov]

- Убрано установка прав доступа на загруженный файл в 664. [Alexander
  Demidov]

- Merge tag 'temp' [Alexander Demidov]

- Merge branch 'master' of dimti.ru:lib/Image. [Alexander Demidov]

- Correct upload with python pil paste. Add Image::getWatermark()
  [Alexander Demidov]

- Merge branch 'master' of dimti.ru:lib/Image. [Alexander Demidov]

- Добавление поддержки в Python PIL указание только одной из сторон для
  ресайзинга. [Alexander Demidov]

- Merge branch 'master' of dimti.ru:lib/Image. [Alexander Demidov]

- Add support pil_options. [Alexander Demidov]

- Изменение .gitignore. [Alexander Demidov]

- MIRSPORTA-73 - Качество картинок. Ресамплинг Изменение прав доступа
  (запись для группы) на файл, сохраняемый при загрузке изображения.
  [Alexander Demidov]

- Merge branch 'master' of dimti.ru:lib/Image. [Alexander Demidov]

- MIRSPORTA-73 - Качество картинок. Ресамплинг. [Alexander Demidov]

- Добавление метода Upload.brightnessContrast(). Добавление метода
  Upload.getFilePath(). [Alexander Demidov]

- Добавление к последнему коммиту. [Alexander Demidov]

- Добавлен параметр $force_create в метод File.getImageVariant().
  Значение по-умолчанию - false. [Alexander Demidov]

- Использование Config::get(PATH_WEB_ROOT) при создании варианта
  изображения в Upload. [Alexander Demidov]

- Автоматическое создание вариантов изображений. [Alexander Demidov]

- Изменение алгоритма ресайзинга (правильный вариант). [Alexander
  Demidov]

- Качество jpeg по-умолчанию - 95. Изменен алгоритм ресайзинга
  (добавление белых полосок) - отключен для изображений-оргиналов. (не
  проверено) [Alexander Demidov]

- Убрано TODO из класса File.getWebName(). Добавлена проверка на
  существование файла и соответсвующее разделение логики в метода
  получения пути к файлу File.getWebName(). [Alexander Demidov]

- Path to dir image cache Upload::dir_image_cache set to public.
  [Alexander Demidov]

- Исправление ошибки связанной с неправильной инициализацией варианта
  изображения. [Alexander Demidov]

- Рефакторинг. Добавлении поддержки параметров, задающих максимально
  допустимые размеры изображения. Изменены параметры для ресайзинга
  изображений (на этот счет добавлено ТОДО). [Alexander Demidov]

- Добавление комментария к методу getIsNoEmpty() и перемещение этого
  метода в класс File. [Alexander Demidov]

- Корректировка использования логгера - если логгинг отключен, тогда
  вывод сообщений об ошибках выводится в стандартный поток вывода ошибок
  в php (если поток не переопределен конфигурационной переменной
  ErrorStream) - пример кода взят с CliController. [Alexander Demidov]

- Добавлено поддержка указания качества результирующего изображения в
  Upload.class. [Alexander Demidov]

- Добавление ТОДО в ImageCollection. [Alexander Demidov]

- Merge branch 'master' of dimti.ru:lib/Image. [Alexander Demidov]

- Добавлено ТОДО. [Alexander Demidov]

- Refactoring use PATH_WEB_ROOT (move to main config). Remove use save
  originals. Add PHP-Doc comment into File class. [Alexander Demidov]

- Set protected static $sizes default value - array(). Correct to
  getting sizes use with static:: [Alexander Demidov]

- Add feature - save original image. Allow create empty instance of
  ImageCollection. [Alexander Demidov]

- Fix bug with wrong if statement. Add TODO with use log error message.
  [Alexander Demidov]

- Allow File::getInstance() with empty data. [Alexander Demidov]

- Add symbolic function getVariant into Image class. [Alexander Demidov]

- Fix bug in File class with wrong parent:: statement. [Alexander
  Demidov]

- Refactoring with use of separate to sub classes from Image. Add
  FileHelper to trying toString conversion. Add ImageCollection (not
  tested work). [Alexander Demidov]

- Modified detect of empty data in File.getInstance(). Add
  SIZE_INSTRUCTION into Image class. [Alexander Demidov]

- Extend Image class. Separate to ImageVariant. Remove _is_new_record.
  [Alexander Demidov]

- Add use PATH_ROOT. Refactoring. Some fixes for checking file_exists,
  is_read, is_file in UploadHelper. [Alexander Demidov]

- Add Image, Upload, UploadHelper. [Alexander Demidov]

- Initial commit. [Alexander Demidov]


