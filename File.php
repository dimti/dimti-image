<?php
namespace dimti\Image;
/**
 * @author Alexander Demidov <demidov@dimti.ru>
 * Full paths to directories and files contains slash as first symbol
 * Dir name not contains slashes on first or last symbols
 */
use Majestic\Config;

/**
 * Class File
 */
abstract class File
{
    public $path;

    public $filename;

    private $logger;

    private $error_stream;

    private $owner;

    public function __construct()
    {

    }

    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function log($message)
    {
        if (\Majestic\Config::get('LOGGING')) {
            if (is_null($this->logger)) {
                $this->logger = \Majestic\Logger\Logger::getInstance();
            }
            $this->logger->log($message);
        } else {
            $this->error_stream = \Majestic\Config::get('ErrorStream', 'php://stderr');
            file_put_contents($this->error_stream, PHP_EOL . 'Log ' . '#' . __CLASS__ . ': ' . $message . PHP_EOL, FILE_APPEND);
        }
    }

    /**
     * @param array|string|null $data
     * @return Image
     * @throws \ErrorException
     */
    public static function getInstance($data = null)
    {
        $instance = new static;
        if (!(is_null($data) || $data == '')) {
            if (!is_array($data)) {
                $data = json_decode($data, true);
                if (json_last_error() != JSON_ERROR_NONE) {
                    $instance->log('Unable to convert json-string to array. Data ' . print_r($data, true));
                }
            }
            if (is_array($data)) {
                foreach ($data as $attribute_name => $attribute_value) {
                    if (property_exists($instance, $attribute_name)) {
                        $instance->{$attribute_name} = $attribute_value;
                    }
                }
            }
        }
        return $instance;
    }

	/**
	 * @param $size
	 * @param bool $force_create
	 * @param bool $important_create
	 *
	 * @return ImageVariant
	 * @throws \ErrorException
	 */
    public function getImageVariant($size, $force_create = false, $important_create = false)
    {
        /**
         * @var $this Image
         */
        if (!array_key_exists($size, $this->variants) || $important_create) {
            $original_file_path = \Majestic\Config::get('PATH_WEB_ROOT') . '/' . $this->getWebName();
            //TODO:Сложно понять логику этого выражения
            if (($important_create && $this->getIsNoEmpty() && file_exists($original_file_path)) || ($force_create && $this->getIsNoEmpty() && file_exists($original_file_path))) {
                Upload::imageVariant($this, $size);
            } else {
                $this->variants[$size] = new ImageVariant();
            }
        } else {
            if (!is_object($this->variants[$size])) {
                $this->variants[$size] = ImageVariant::getInstance($this->variants[$size]);
            }
        }
        return $this->variants[$size];
    }

    /**
     * Если объект содержит информацию о файле/изображение вернет - true
     * Если объект пуст - вернет false
     * @return bool
     */
    public function getIsNoEmpty()
    {
        return (bool) $this->size;
    }

	public function delete()
	{
		/**
		 * @var $image Image
		 */
		if ($this->getIsNoEmpty()) {
            @unlink($this->getRealPath());
		}
		if (get_parent_class($this) == 'dimti\Image\Image') {
			$image = $this;
			foreach ($image->getSizes() as $size) {
				$image_variant = $image->getVariant($size);
				if ($image_variant->getIsNoEmpty()) {
					@unlink($image_variant->getRealPath());
				}
			}
		}
	}

	public function getRealPath()
	{
		return Config::get('PATH_WEB_ROOT') . '/' . $this->path . '/' . $this->filename;
	}

    /**
     * @return string
     */
    public function getWebName()
    {
        return (($this->getIsNoEmpty()) ? ($this->path . '/' . $this->filename) : '_.gif');
    }

    public function __toString()
    {
        return FileHelper::toString($this);
    }
}