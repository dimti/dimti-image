<?php
namespace dimti\Image;

class FileHelper
{
    public static function toString($object)
    {
        $data = array();
        foreach ($object as $attribute_name => $attribute_value) {
            $data[$attribute_name] = $attribute_value;
        }
        return json_encode($data);
    }
}