<?php
namespace dimti\Image;

abstract class Image extends File
{
    public $width;
    public $height;
    public $type;
    public $size;

    public $original_filename;

    /**
     * @var ImageVariant[]
     */
    public $variants = array();

    protected static $max_width;

    protected static $max_height;

    protected static $sizes = array();

    protected static $watermarks = array();

    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * @param $class string|\StdClass
     * @throws \ErrorException
     */
    public static function checkSubClass($class)
    {
        if (!self::getIsSubClass($class)) {
            throw new \ErrorException('Class "' . $class . '" mast be extend of "Image".');
        }
    }

    /**
     * @param $class string|\StdClass
     * @return bool
     */
    public static function getIsSubClass($class)
    {
        $parent_class = get_parent_class($class);
        if ($parent_class == __CLASS__) {
            return true;
        }
        if (get_parent_class($parent_class)) {
            return self::getIsSubClass($parent_class);
        }
        return false;
    }

    public static function getSizes()
    {
        return static::$sizes;
    }

    public static function getWatermark($size)
    {
        return (isset(static::$watermarks[$size])) ? static::$watermarks[$size] : false;
    }

    public static function getMaxWidth()
    {
        return static::$max_width;
    }

    public static function getMaxHeight()
    {
        return static::$max_height;
    }

	/**
	 * @param $size
	 * @param bool $force_create
	 * @param bool $important_create
	 *
	 * @return ImageVariant
	 */
    public function getVariant($size, $force_create = false, $important_create = null)
    {
        if ($force_create && $important_create === null && is_array($this->variants) && array_key_exists($size, $this->variants) && is_array($this->variants[$size])
            && $this->variants[$size]['size'] === null) {
            $important_create = true;
        }

        return parent::getImageVariant($size, $force_create, $important_create);
    }

    public function getThumb($width = 0, $height = 0, $crop_method = null) {
        if ( $width == 0 && $height == 0 ) {
            return \Majestic\Config::get('static_url') . $this->getWebName();
        }
        return \Majestic\Config::get('static_url') . $this->getVariant($width . 'x' . $height)->getWebName();
    }
}