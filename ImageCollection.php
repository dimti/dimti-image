<?php
namespace dimti\Image;

abstract class ImageCollection extends \ArrayIterator
{
    /**
     * @var string
     */
    protected $class;

    /**
     * @param array|null $data
     * @param mixed $owner
     * @throws \ErrorException
     * TODO: рефакторить этот метод
     */
    public function __construct($data = null, $owner = null)
    {
        /**
         * @var $class Image
         */
        $class = $this->class;
        Image::checkSubClass($class);
        if (!is_null($data) && $data != '') {
            if (!is_array($data)) {
                $data = json_decode($data, true);
                if (json_last_error() != JSON_ERROR_NONE) {
                    \ErrorMessage::log('Unable to convert json-string to array.');
                    return;
                }
            }
            if (!isset($data['order']) || !isset($data['files'])) {
                \ErrorMessage::log('Incorrect format for create ImageCollection.');
                return;
            }
            foreach ($data['order'] as $original_filename) {
                if (!isset($data['files'][$original_filename])) {
                    \ErrorMessage::log('Unable to fetch image "' . $original_filename . '" from ImageCollection.');
                    continue;
                }
                $image_instance = $class::getInstance($data['files'][$original_filename]);
                if ($owner) {
                    $image_instance->setOwner($owner);
                }
                $this->append($image_instance);
            }
        }
        return;
    }

    public function __toString()
    {
        $image_collection = array(
            'order' => array(),
            'files' => array()
        );
        foreach ((array)$this as $image) {
            $image_collection['order'][] = $image->original_filename;
            $image_collection['files'][$image->original_filename] = (string)$image;
        }
        return json_encode($image_collection);
    }
}