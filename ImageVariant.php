<?php
namespace dimti\Image;

class ImageVariant extends File
{
    public $width;
    public $height;
    public $type;
    public $size;

    /**
     * @param $class string|\StdClass
     * @return mixed
     */
    public static function getIsClass($class)
    {
        if (is_object($class)) {
            return (get_class($class) == __CLASS__);
        } else {
            return ($class == __CLASS__);
        }
    }
}