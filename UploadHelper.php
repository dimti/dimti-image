<?php
namespace dimti\Image;

abstract class UploadHelper
{
    /**
     * @param $proportion
     * @param $file_path
     * @return bool
     * @throws \ErrorException
     */
    public static function imageCompareAspectRatio($proportion, $file_path)
    {
        if (!(file_exists($file_path) && is_readable($file_path) && is_file($file_path))) {
            throw new \ErrorException('Unable to read file "' . $file_path . '".');
        }
        if ($imagesize = getimagesize($file_path)) {
            $proprtion_parts = explode('x', $proportion);
            if ($proprtion_parts[0] / $proprtion_parts[1] === $imagesize[0] / $imagesize[1]) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $size
     * @param $file_path
     * @return bool
     * @throws \ErrorException
     */
    public static function imageCompareSize($size, $file_path)
    {
        if (!(file_exists($file_path) && is_readable($file_path) && is_file($file_path))) {
            throw new \ErrorException('Unable to read file "' . $file_path . '".');
        }
        if ($imagesize = getimagesize($file_path)) {
            $size_parts = explode('x', $size);
            if ($imagesize[0] == $size_parts[0] && $imagesize[1] == $size_parts[1]) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $size
     * @param $file_path
     * @return bool
     */
    public static function imageCheckMinSize($size, $file_path)
    {
        $imagesize = getimagesize($file_path);
        $size_parts = explode('x', $size);
        if ($imagesize[0] >= $size_parts[0] && $imagesize[1] >= $size_parts[1]) {
            return true;
        } else {
            return false;
        }
    }
}